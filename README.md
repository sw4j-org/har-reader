# Introduction

This project is a HAR (HTTP Archive) reader to read and write HAR files.

Further informations can be found at the [documentation page](https://har-reader.sw4j.org/).
