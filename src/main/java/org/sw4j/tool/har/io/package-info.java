/**
 * This package contains all classes to work with the model and persistent presentations of the model (e.g. files).
 */
package org.sw4j.tool.har.io;
