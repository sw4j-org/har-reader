/**
 * <p>
 * This package contains the HAR model. The container class is {@link Har}.
 * </p>
 * <p>
 * The classes of this package are not thread safe.
 * </p>
 */
package org.sw4j.tool.har.model;
